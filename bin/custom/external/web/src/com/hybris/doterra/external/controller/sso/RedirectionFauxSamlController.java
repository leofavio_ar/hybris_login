/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.hybris.doterra.external.controller.sso;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.persistence.security.EJBPasswordEncoderNotFoundException;
import com.hybris.doterra.external.controller.sso.SamlsinglesignonConstants;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 */
@Controller
@RequestMapping("/faux/*")
//@RequestMapping("/faux/**")
public class RedirectionFauxSamlController
{

	private final static Logger LOGGER = Logger.getLogger(RedirectionFauxSamlController.class);

	@Resource(name = "userService")
	private UserService userService;

	@RequestMapping(value = "/agent", method = RequestMethod.GET)
	public RedirectView redirectAgent(final HttpServletResponse response, final HttpServletRequest request)
	{

		UserModel user;

		String referenceURL = StringUtils.substringAfter(request.getServletPath(), "/faux/");

		if (!StringUtils.isEmpty(request.getQueryString()))
		{
			referenceURL += request.getQueryString().isEmpty() ? "" : "?" + request.getQueryString();
		}

        RedirectView redirectView = new RedirectView();

		try
		{

			user = userService.getUserForUID("asagent");

			storeTokenFromSaml(response, user);

			//continue to the redirection and token will be generated only in case the user has valid group

            final String redirectURL = StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_REDIRECT_URL),
					SamlsinglesignonConstants.DEFAULT_REDIRECT_URL);

            redirectView.setUrl("https://localhost/" + referenceURL);

			LOGGER.info("About to be redirected from SAML redirect.");
			return redirectView;

		}
		catch (final IllegalArgumentException e)
		{
			//the user is not belonging to any valid group
			LOGGER.error(e);
		}

		catch (final Exception e)
		{
			//something went wrong and we need to log that
			LOGGER.error(e);
		}
        redirectView.setUrl("/error");
		return redirectView;
	}


    //@RequestMapping(value = "/customer", method = RequestMethod.GET)
    /*public RedirectView redirectCustomer(final HttpServletResponse response, final HttpServletRequest request)
    {

        UserModel user;

        String referenceURL = StringUtils.substringAfter(request.getServletPath(), "/faux/");

        if (!StringUtils.isEmpty(request.getQueryString()))
        {
            referenceURL += request.getQueryString().isEmpty() ? "" : "?" + request.getQueryString();
        }

        RedirectView redirectView = new RedirectView();

        try
        {

            user = userService.getUserForUID("1161");

            storeTokenFromJwt(response, user);

            //continue to the redirection and token will be generated only in case the user has valid group

            final String redirectURL = StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_REDIRECT_URL),
                    SamlsinglesignonConstants.DEFAULT_REDIRECT_URL);

            redirectView.setUrl("https://localhost/" + referenceURL);

            LOGGER.info("About to be redirected from SAML redirect.");
            return redirectView;

        }
        catch (final IllegalArgumentException e)
        {
            //the user is not belonging to any valid group
            LOGGER.error(e);
        }

        catch (final Exception e)
        {
            //something went wrong and we need to log that
            LOGGER.error(e);
        }
        redirectView.setUrl("/error");
        return redirectView;
    }*/


    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public String redirectCustomer(final HttpServletResponse response, final HttpServletRequest request)
    {

        UserModel user;

        String referenceURL = StringUtils.substringAfter(request.getServletPath(), "/faux/");

        if (!StringUtils.isEmpty(request.getQueryString()))
        {
            referenceURL += request.getQueryString().isEmpty() ? "" : "?" + request.getQueryString();
        }

        RedirectView redirectView = new RedirectView();

        try
        {

            user = userService.getUserForUID("1161");

            storeTokenFromJwt(response, user);

            //continue to the redirection and token will be generated only in case the user has valid group

            final String redirectURL = StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_REDIRECT_URL),
                    SamlsinglesignonConstants.DEFAULT_REDIRECT_URL);

            redirectView.setUrl("https://powertools.local:9002/" + referenceURL);

            LOGGER.info("About to be redirected from SAML redirect.");
            //return redirectView;
            return "redirect:https://powertools.local:9002/yb2bacceleratorstorefront/powertools/en/USD";

        }
        catch (final IllegalArgumentException e)
        {
            //the user is not belonging to any valid group
            LOGGER.error(e);
        }

        catch (final Exception e)
        {
            //something went wrong and we need to log that
            LOGGER.error(e);
        }
        redirectView.setUrl("/error");
        return "";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public RedirectView redirectAdmin(final HttpServletResponse response, final HttpServletRequest request)
    {

        UserModel user;

        String referenceURL = StringUtils.substringAfter(request.getServletPath(), "/faux/");

        if (!StringUtils.isEmpty(request.getQueryString()))
        {
            referenceURL += request.getQueryString().isEmpty() ? "" : "?" + request.getQueryString();
        }

        RedirectView redirectView = new RedirectView();

        try
        {

            user = userService.getUserForUID("admin");

            storeTokenFromJwt(response, user);

            //continue to the redirection and token will be generated only in case the user has valid group

            final String redirectURL = StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_REDIRECT_URL),
                    SamlsinglesignonConstants.DEFAULT_REDIRECT_URL);

            redirectView.setUrl("https://localhost/" + referenceURL);

            LOGGER.info("About to be redirected from SAML redirect.");
            return redirectView;

        }
        catch (final IllegalArgumentException e)
        {
            //the user is not belonging to any valid group
            LOGGER.error(e);
        }

        catch (final Exception e)
        {
            //something went wrong and we need to log that
            LOGGER.error(e);
        }
        redirectView.setUrl("/error");
        return redirectView;
    }

	public void storeTokenFromSaml(final HttpServletResponse response, final UserModel user)
	{
		try
		{

			final String cookiePath = StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_PATH),
					SamlsinglesignonConstants.DEFAULT_COOKIE_PATH);

			final String cookieMaxAgeStr = StringUtils.defaultIfEmpty(
					Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_MAX_AGE),
					String.valueOf(SamlsinglesignonConstants.DEFAULT_COOKIE_MAX_AGE));

			int cookieMaxAge;

			if (!NumberUtils.isNumber(cookieMaxAgeStr))
			{
				cookieMaxAge = SamlsinglesignonConstants.DEFAULT_COOKIE_MAX_AGE;
			}
			else
			{
				cookieMaxAge = Integer.valueOf(cookieMaxAgeStr).intValue();
			}

			UserManager.getInstance().storeLoginTokenCookie(
					//
					StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_NAME),
							SamlsinglesignonConstants.SSO_DEFAULT_COOKIE_NAME), // cookie name
					user.getUid(), // user id
					"en", // language iso code
					null, // credentials to check later
					cookiePath, // cookie path
					StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_DOMAIN),
							SamlsinglesignonConstants.SSO_DEFAULT_COOKIE_DOMAIN), // cookie domain
					true, // secure cookie
					cookieMaxAge, // max age in seconds
					response);
		}
		catch (final EJBPasswordEncoderNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}


    public void storeTokenFromJwt(final HttpServletResponse response, final UserModel user)
    {
        try
        {

            final String cookiePath = StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_PATH),
                    SamlsinglesignonConstants.DEFAULT_COOKIE_PATH);

            final String cookieMaxAgeStr = StringUtils.defaultIfEmpty(
                    Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_MAX_AGE),
                    String.valueOf(SamlsinglesignonConstants.DEFAULT_COOKIE_MAX_AGE));

            int cookieMaxAge;

            if (!NumberUtils.isNumber(cookieMaxAgeStr))
            {
                cookieMaxAge = SamlsinglesignonConstants.DEFAULT_COOKIE_MAX_AGE;
            }
            else
            {
                cookieMaxAge = Integer.valueOf(cookieMaxAgeStr).intValue();
            }

            UserManager.getInstance().storeLoginTokenCookie(
                    //
                    "LoginToken", // cookie name
                    user.getUid(), // user id
                    "en", // language iso code
                    null, // credentials to check later
                    cookiePath, // cookie path
                    StringUtils.defaultIfEmpty(Config.getParameter(SamlsinglesignonConstants.SSO_COOKIE_DOMAIN),
                            SamlsinglesignonConstants.SSO_DEFAULT_COOKIE_DOMAIN), // cookie domain
                    true, // secure cookie
                    cookieMaxAge, // max age in seconds
                    response);
        }
        catch (final EJBPasswordEncoderNotFoundException e)
        {
            throw new RuntimeException(e);
        }
    }

}
