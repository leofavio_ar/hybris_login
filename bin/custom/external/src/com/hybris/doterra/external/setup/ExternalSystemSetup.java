/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.doterra.external.setup;

import static com.hybris.doterra.external.constants.ExternalConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.hybris.doterra.external.constants.ExternalConstants;
import com.hybris.doterra.external.service.ExternalService;


@SystemSetup(extension = ExternalConstants.EXTENSIONNAME)
public class ExternalSystemSetup
{
	private final ExternalService externalService;

	public ExternalSystemSetup(final ExternalService externalService)
	{
		this.externalService = externalService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		externalService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return ExternalSystemSetup.class.getResourceAsStream("/external/sap-hybris-platform.png");
	}
}
